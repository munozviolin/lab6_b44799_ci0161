package cr.ac.ucr.ecci.cql.mipatrones;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Handler;

// Capa de Negocios (Presenter o Controller)
// Implementacion de GetListItemsInteractor de la capa de negocio (P o M) para obtener los resultados de la lista de elementos a mostrar
// Representa el Interactor (casos de uso), se comunica con las entidades y el presenter
public class GetListItemsInteractorImpl implements GetListItemsInteractor {
    private ItemsRepository mItemsRepository;
    private Context context;

    public GetListItemsInteractorImpl(Context applicationContext) {
        this.context=applicationContext;
    }

    @Override public void getItems(final OnFinishedListener listener) throws DataBaseDataSource.CantRetrieveItemsException {
// Enviamos el hilo de ejecucion con un delay para mostar la barra de progreso
        //new Handler().postDelayed(new Runnable() {
            //@Override public void run() {
                List<Persona> itemsTemp = null;
                //List<String> items =  new ArrayList<String>();;
                mItemsRepository = new ItemsRepositoryImpl(context);
                itemsTemp = mItemsRepository.obtainItems();
                /*try {
                    // obtenemos los items
                    itemsTemp = mItemsRepository.obtainItems();
                    for (int i = 0; i < itemsTemp.size(); i++){
                        items.add(itemsTemp.get(i).getNombre());
                    }
                } catch (DataBaseDataSource.CantRetrieveItemsException e) {
                    e.printStackTrace();
                }*/
                // Al finalizar retornamos los items
                listener.onFinished(itemsTemp);
            //}
        //}, 2000);
    }
}