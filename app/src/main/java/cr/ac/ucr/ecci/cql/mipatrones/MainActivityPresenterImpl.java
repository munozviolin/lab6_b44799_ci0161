package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;
import android.content.Intent;

import java.util.List;
// Capa de presentacion (Presenter)
// Presenter (P) -> Implementacion de MainActivityPresenter y de GetListItemsInteractor.OnFinishedListener
public class MainActivityPresenterImpl implements
        // Implementacion de MainActivityPresenter de la Capa (P)
        MainActivityPresenter,
        // La interface OnFinishedListener de GetListItemsInteractor para obtener el valor de retorno de la capa Iteractor (P)
        GetListItemsInteractor.OnFinishedListener {
    private MainActivityView mMainActivityView;
    private GetListItemsInteractor mGetListItemsInteractor;

    private Context context;

    public MainActivityPresenterImpl(MainActivityView mainActivityView, Context applicationContext) {
        this.mMainActivityView = mainActivityView;
        // Capa de negocios (Interactor)
        this.mGetListItemsInteractor = new GetListItemsInteractorImpl(applicationContext);
        context = applicationContext; //
    }

    @Override
    public void onResume() throws DataBaseDataSource.CantRetrieveItemsException {
        if (mMainActivityView != null) {
            mMainActivityView.showProgress();
        }
        // Obtener los items de la capa de negocios (Interactor)
        mGetListItemsInteractor.getItems(this);
    }

    // Evento de clic en la lista
    @Override
    public void onItemClicked(int position) {
        ItemsRepository mItemsRepository = new ItemsRepositoryImpl(context);
        List<Persona> items = null;
        try {
            items = mItemsRepository.obtainItems();
        } catch (DataBaseDataSource.CantRetrieveItemsException e) {
            e.printStackTrace();
        }

        if (mMainActivityView != null) {
            Intent intent = new Intent(context, DetallesPersona.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Persona", items.get(position));
            context.startActivity(intent);
            //mMainActivityView.showMessage(String.format("Position %d clicked", position + 1));
        }
    }

    @Override
    public void onDestroy() {
        mMainActivityView = null;
    }

    @Override
    public void onFinished(List<Persona> items) {
        if (mMainActivityView != null) {
            mMainActivityView.setItems(items);
            mMainActivityView.hideProgress();
        }
    }

    // Retornar la vista
    public MainActivityView getMainView() {
        return mMainActivityView;
    }
}