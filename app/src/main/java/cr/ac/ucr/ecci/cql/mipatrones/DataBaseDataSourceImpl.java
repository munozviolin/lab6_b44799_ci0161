package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;

import java.util.Arrays;
import java.util.List;
// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public class DataBaseDataSourceImpl implements DataBaseDataSource {
    Context context;
    public DataBaseDataSourceImpl(Context context) {
        this.context = context;
    }

    @Override
    public List<Persona> obtainItems() throws BaseDataItemsException {
        List<Persona> items = null;
        try {
            // TODO: Obtener de la base de datos
            items = createArrayList();
        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }
        return items;
    }
    // Esta lista debe recuperarla de la base de datos
    // para el ejemplo la inicializamos con datos dummy
    private List<Persona> createArrayList() {
        List<Persona> objectList = Arrays.asList(new Persona[3]);;
        Persona persona = new Persona();
        Persona persona2 = new Persona();
        Persona persona3 = new Persona();
        persona.leer(context, "B42424");
        objectList.set(0, persona);
        persona2.leer(context, "B64646");
        objectList.set(1, persona2);
        persona3.leer(context, "C00195");
        objectList.set(2, persona3);

        return objectList;
    }
}