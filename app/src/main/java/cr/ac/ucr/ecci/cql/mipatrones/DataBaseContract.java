package cr.ac.ucr.ecci.cql.mipatrones;

import android.provider.BaseColumns;

//contrato de la base de datos
public class DataBaseContract {

    private DataBaseContract() {
    }

    public static class DataBaseEntry implements BaseColumns {
        //nombre de la tabla
        public static final String TABLE_NAME_PERSONA = "Persona";

        //nombre de las columnas
        public static final String COLUMN_NAME_IDENTIFICACION = "identificacion"; //llave primaria de la tabla
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_IDIMAGEN = "idimagen";
    }

    // Crear las tablas de la base de datos
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_PERSONA =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_PERSONA + " (" +
                    DataBaseEntry.COLUMN_NAME_IDENTIFICACION + TEXT_TYPE + " PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_IDIMAGEN + TEXT_TYPE + " )";

    //borrar la tabla de la base de datos
    public static final String SQL_DELETE_PERSONA = "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_PERSONA;

}
