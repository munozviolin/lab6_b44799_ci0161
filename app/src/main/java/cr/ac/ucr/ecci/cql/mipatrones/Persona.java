package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

//configuracion del objeto TableTop mediante la implementacion parcelable para que pueda pasarse como objeto entre clases
public final class Persona implements Parcelable{
    private String identificacion;
    private String nombre;
    private String idimagen;

    protected Persona(Parcel in) {
        identificacion = in.readString();
        nombre = in.readString();
        idimagen = in.readString();
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(identificacion);
        parcel.writeString(nombre);
        parcel.writeString(idimagen);
    }

    public Persona() {}

    public Persona(String identificacion, String nombre, String idimagen) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.idimagen = idimagen;
    }

    //configuracion del metodo insertar en la base de datos
    public long insertar(Context context) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put(DataBaseContract.DataBaseEntry._ID, getIdentificacion());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_IDENTIFICACION, getIdentificacion());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE, getNombre());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_IDIMAGEN, getIdImagen());

        return db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA, null,
                values);
    }

    //configuracion del metodo leer en la base de datos
    public void leer (Context context, String id) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        String[] projection = {
                DataBaseContract.DataBaseEntry.COLUMN_NAME_IDENTIFICACION,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_IDIMAGEN
        };
        //WHERE
        String selection = DataBaseContract.DataBaseEntry.COLUMN_NAME_IDENTIFICACION + " = ?";
        String[] selectionArgs = {id};
        //String query = selection + selectionArgs[0];
        Cursor cursor = db.query(
                DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA, // tabla
                projection, // columnas
                selection, // where
                selectionArgs, // valores del where
        null,
                null,
        null );
        //cursor.moveToFirst();
        //cursor.getCount() > 0
        if(cursor.moveToFirst()){
            setIdentificacion(cursor.getString(cursor.getColumnIndexOrThrow(
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_IDENTIFICACION)));
            setNombre(cursor.getString(cursor.getColumnIndexOrThrow(
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE)));
            setIdImagen(cursor.getString(cursor.getColumnIndexOrThrow(
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_IDIMAGEN)));
        }
        cursor.close();
    }

    //setters y getters de cada atributo de la tabla de la base de datos
    public String getIdentificacion() {
        return identificacion;
    }
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getIdImagen() {
        return idimagen;
    }
    public void setIdImagen(String idImagen) {
        this.idimagen = idImagen;
    }
}