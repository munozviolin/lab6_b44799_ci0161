package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 4; //version de la base de datos
    public static final String DATABASE_NAME = "AndroidStorage.db"; //nombre de la base de datos

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //creacion
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DataBaseContract.SQL_CREATE_PERSONA);
    }

    //actualizacion
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DataBaseContract.SQL_DELETE_PERSONA);
        onCreate(db);
    }

    //devolverse a version anterior de la base de datos
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}