package cr.ac.ucr.ecci.cql.mipatrones;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

public class DetallesPersona extends AppCompatActivity {

    String detalles;
    String imagen;
    Persona persona;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_persona);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        persona = getIntent().getExtras().getParcelable("Persona");
        detalles = "Carnet: " + persona.getIdentificacion() + "\n" + "Nombre: " + persona.getNombre();
        imagen = persona.getIdImagen();
    }

    public Persona getPersona(){
        return persona;
    }

    public String getDetalles(){ return detalles; }

    public String getImagen(){ return imagen; }

}
