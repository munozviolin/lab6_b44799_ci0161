package cr.ac.ucr.ecci.cql.mipatrones;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class FragmentoDetalles extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_detalles, container, false);
        DetallesPersona detallesPersona = (DetallesPersona) getActivity();
        String row = detallesPersona.getDetalles();
        TextView data = rootView.findViewById(R.id.textview_first);
        data.setText(row);
        ImageView imagen = rootView.findViewById(R.id.imageView);
        Context context = imagen.getContext();
        int id = context.getResources().getIdentifier(detallesPersona.getImagen(), "drawable", context.getPackageName());
        imagen.setImageResource(id);
        return rootView;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
