package cr.ac.ucr.ecci.cql.mipatrones;

import java.util.List;
// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valroes
public interface ItemsRepository {
    List<Persona> obtainItems() throws
            DataBaseDataSource.CantRetrieveItemsException;
}